#include <Arduino.h>
#include <ros.h>
#include <sensor_msgs/Range.h>

ros::NodeHandle nh;

sensor_msgs::Range us_a_range_msg;
sensor_msgs::Range us_b_range_msg;
sensor_msgs::Range ir_range_msg;

ros::Publisher us_a_pub_range("ultrasound_a", &us_a_range_msg);
ros::Publisher us_b_pub_range("ultrasound_b", &us_b_range_msg);
ros::Publisher ir_pub_range("infrared", &ir_range_msg);

const int us_a_trig_pin = 8;
const int us_a_echo_pin = 9;

const int us_b_trig_pin = 6;
const int us_b_echo_pin = 7;

const int analog_pin = 0;

long duration;
int distance;

char us_a_frameid[] = "/us_a_ranger";
char us_b_frameid[] = "/us_b_ranger";
char ir_frameid[] = "/ir_ranger";

unsigned long range_timer;
unsigned int inverval = 500; // ms

float irGetRange(int pin_num) {
    float a = 4903.255;
    float b = -1.05376;
    int sample = analogRead(pin_num);
    float y = a * pow(sample, b);
    return y;
}

float usGetRange(int us_a_echo_pin, int us_a_trig_pin) {
  digitalWrite(us_a_trig_pin, LOW);
  delayMicroseconds(2);

  digitalWrite(us_a_trig_pin, HIGH);
  delayMicroseconds(10);
  digitalWrite(us_a_trig_pin, LOW);

  duration = pulseIn(us_a_echo_pin, HIGH);

  distance = duration*0.034/2;
  return distance;
}

void setup() {
  nh.initNode();
  nh.advertise(us_a_pub_range);
  nh.advertise(us_b_pub_range);
  nh.advertise(ir_pub_range);

  pinMode(us_a_trig_pin, OUTPUT);
  pinMode(us_a_echo_pin, INPUT);

  pinMode(us_b_trig_pin, OUTPUT);
  pinMode(us_b_echo_pin, INPUT);

  us_a_range_msg.radiation_type = sensor_msgs::Range::ULTRASOUND;
  us_a_range_msg.header.frame_id =  us_a_frameid;
  us_a_range_msg.field_of_view = 0.01;
  us_a_range_msg.min_range = 2;
  us_a_range_msg.max_range = 80;

  us_b_range_msg.radiation_type = sensor_msgs::Range::ULTRASOUND;
  us_b_range_msg.header.frame_id =  us_b_frameid;
  us_b_range_msg.field_of_view = 0.01;
  us_b_range_msg.min_range = 2;
  us_b_range_msg.max_range = 80;

  ir_range_msg.radiation_type = sensor_msgs::Range::INFRARED;
  ir_range_msg.header.frame_id =  ir_frameid;
  ir_range_msg.field_of_view = 0.01;
  ir_range_msg.min_range = 2;
  ir_range_msg.max_range = 80;
}

void loop() {
  if (millis() > range_timer) {

    us_a_range_msg.range = usGetRange(us_a_echo_pin, us_a_trig_pin);
    us_a_range_msg.header.stamp = nh.now();
    us_a_pub_range.publish(&us_a_range_msg);

    us_b_range_msg.range = usGetRange(us_b_echo_pin, us_b_trig_pin);
    us_b_range_msg.header.stamp = nh.now();
    us_b_pub_range.publish(&us_b_range_msg);

    ir_range_msg.range = irGetRange(analog_pin);
    ir_range_msg.header.stamp = nh.now();
    ir_pub_range.publish(&ir_range_msg);

    range_timer =  millis() + inverval;
  }

  nh.spinOnce();
}
